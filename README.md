## 이미지 올리는 방법

1. 이미지를 용도에 맞는 경로 확인하고 업로드 한다.

   1. 배너 이미지
      - https://gitlab.com/albit-public/public/tree/master/banners/images
   2. 웰컴팝업 이미지
      - https://gitlab.com/albit-public/public/tree/master/public/images

2. 올린 이미지의 URL 을 따서 용도에 맞게 사용하기.
   - 이미지를 주소창에 드래그 드랍하면 나옴.
   - 예시 -> https://gitlab.com/albit-public/public/raw/master/banners/images/xxx.xxx
   - /public/ 폴더 안의 내용은 아래 url 형식으로 사용 가능함.
     https://albit-public.gitlab.io/public/images/sample.jpg

## 하단 배너 데이타 작성 하는 방법

1. 공지사항을 작성 in admin web page
2. 공지사항의 id 를 따로 적어둔다. - 게시물 좌측의 번호 -
3. 연결할 배너 이미지를 업로드 한다.
   - https://gitlab.com/albit-public/public/tree/master/banners/images
4. 올린 이미지의 URL 을 따온다.
   - 이미지를 주소창에 드래그 드랍하면 나옴.
   - 예시 -> https://gitlab.com/albit-public/public/raw/master/banners/images/xxx.xxx
5. 배너 데이타 파일을 편집한다.
   - https://gitlab.com/albit-public/public/edit/master/banners/index.json
6. 형식에 맞추어 배너와 공지사항 내용을 작성한다. 기존 아이템을 복사하면 편리함.
   모두 작성 후, 하단의 'Commit changes' 클릭

```
{
  "image": "https://url-to-banner-image.jpg",
  "action": "OpenNotice",
  "data": id-for-this-notice
},

```

## 공지사항 링크 만드는 방법

1. 아래 위치에 공지 내용을 작성하여 올린다.

- https://gitlab.com/albit-public/public/tree/master/public/notices
- 내용 (html)을 아래 틀에 붙여서 작성한다. (~~~ 을 바꿔치기 한다.)

```
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
  </head>
  <body style="padding:5%">

    ~~~

  </body>
</html>
```

- 모두 작성 후, 하단의 'Commit changes' 클릭

2. url 형식은 이렇다.

```
  https://albit-public.gitlab.io/public/notices/notice-sample.html
```

## 웰컴팝업 만들기

1. FirebaseRemoteConfig Open
   https://console.firebase.google.com/u/0/project/admob-app-id-1911172394/config
2. 경로 - welcomePopupItems/items

```sample
    {
      "title": "단기일자리 자세히 보기",
      "image": "https://firebasestorage.googleapis.com/v0/b/admob-app-id-1911172394.appspot.com/o/event-images%2Fevent-hire-testers.png?alt=media&token=1c64cd0a-f183-4876-a095-b9c43519e8ea",
      "link": "https://albit-public.gitlab.io/public/notices/gig-21.html",
      "begin": "2019.12.17 00:00:00",
      "end": "2019.12.23 23:59:59",
      "platforms": [
        "android",
        "iOS"
      ]
    },
```

- title: 하단 버튼 제목
- image: 이미지 경로
- link: 버튼 누를 경우 이동되는 경로
- begin: 게시 시작 시간
- end: 게시 종료 시간
- platforms: ["android", "iOS"]

1. 주의 사항
   1. platforms 에는 정확하게 "android", "iOS" 라고 표기 해야 함
      1. 반영되는 케이스: ["android", "iOS"]
      2. 제외되는 케이스(편의를 위해 고의로 사용하기도 함): ["Android(대소문자 구분함)", "ios(대소문자 구분함)", "!android", "xandroid", "and roid", "안드로이드"]
   2. 데이타 반영 시간
      1. DEV 는, 앱을 백그라운드에서 날리고 다시 시작하면 바로 반영되나, PRD 는 최대 30분 정도 시간 소요됨.
   3. 수정 후 아래 절차대로 버튼을 클릭해줘야 함.
      1. 해당 데이타셋 편집 후, 업데이트 클릭
      2. 이후 메인 리스트 화면에서 우상단 "변경사항 게시" 반드시 클릭해야 함.
